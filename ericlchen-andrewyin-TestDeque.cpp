// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, iterator_star1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(70);
    iterator b = begin(x);
    iterator e = end(x);
    e--;
    *e = 5;
    ASSERT_EQ(x.size(), 70);
    ASSERT_EQ(*b, 0);
    ASSERT_EQ(*e, 5);
    e--;
    ASSERT_EQ(*e, 0);
}

TYPED_TEST(DequeFixture, iterator_star2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(1);
    iterator e = end(x);
    e--;
    --(*e);
    ASSERT_EQ(x.size(), 1);
    ASSERT_EQ(*e, -1);
}

TYPED_TEST(DequeFixture, iterator_star3) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(5);
    iterator b = begin(x);
    ASSERT_EQ(*b, 0);
}

TYPED_TEST(DequeFixture, iterator_plus_minus1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(70);
    iterator b = begin(x);
    iterator e = end(x);
    for(int i = 0; i < 70; i++) {
        ++b;
    }
    EXPECT_TRUE(b == e);
    for(int i = 0; i < 70; i++) {
        --e;
    }
    EXPECT_TRUE(e == x.begin());
}

TYPED_TEST(DequeFixture, iterator_plus_minus2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(50);
    iterator b = begin(x);
    iterator e = end(x);
    x.push_back(4);
    for(int i = 0; i < 50; i++) {
        ++b;
    }
    EXPECT_TRUE(b == e);
    EXPECT_TRUE(++b == x.end());
}

TYPED_TEST(DequeFixture, iterator_plus_minus3) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(50);
    iterator b1 = begin(x);
    x.push_front(4);
    iterator b2 = begin(x);
    EXPECT_TRUE(++b2 == b1);
}

TYPED_TEST(DequeFixture, iterator_plus_equals1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6});
    iterator b = begin(x);
    b += 3;
    ASSERT_EQ(*b, 4);
}

TYPED_TEST(DequeFixture, iterator_plus_equals2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6});
    iterator b = begin(x);
    b += 3;
    b += -1;
    ASSERT_EQ(*b, 3);
}

TYPED_TEST(DequeFixture, iterator_plus_equals3) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6});
    iterator e = end(x);
    e += -3;
    ASSERT_EQ(*e, 4);
}

TYPED_TEST(DequeFixture, iterator_minus_equals1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6});
    iterator e = end(x);
    e -= 6;
    ASSERT_EQ(*e, 1);
}

TYPED_TEST(DequeFixture, iterator_minus_equals2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6});
    iterator e = end(x);
    e -= 6;
    e -= -2;
    ASSERT_EQ(*e, 3);
}

TYPED_TEST(DequeFixture, iterator_minus_equals3) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6});
    iterator b = begin(x);
    b -= -2;
    ASSERT_EQ(*b, 3);
}

TYPED_TEST(DequeFixture, iterator_minus_equals4) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6});
    iterator e = end(x);
    e -= 2;
    e -= 2;
    e -= 2;
    ASSERT_EQ(*e, 1);
}

TYPED_TEST(DequeFixture, push_front) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(70, 2);
    iterator b = begin(x);
    x.push_front(4);
    x.push_front(6);
    ASSERT_EQ(x.size(), 72);
    ASSERT_EQ(*b, 2);
    iterator b2 = begin(x);
    ASSERT_EQ(*b2, 6);
    ++b2;
    ASSERT_EQ(*b2, 4);
    ++b2;
    ASSERT_EQ(*b2, 2);
    iterator e = end(x);
    --e;
    ASSERT_EQ(*e, 2);
}

TYPED_TEST(DequeFixture, push_back) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(99, 2);
    iterator b = begin(x);
    iterator e = end(x);
    x.push_back(4);
    x.push_back(6);
    ASSERT_EQ(x.size(), 101);
    ASSERT_EQ(*b, 2);
    --e;
    ASSERT_EQ(*e, 2);
    iterator e2 = end(x);
    --e2;
    ASSERT_EQ(*e2, 6);
    --e2;
    ASSERT_EQ(*e2, 4);
}

TYPED_TEST(DequeFixture, pop_front) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(99, 2);
    iterator e = end(x);
    --e;
    x.push_front(4);
    x.push_front(6);
    ASSERT_EQ(x.size(), 101);
    x.pop_front();
    iterator b1 = begin(x);
    ASSERT_EQ(*b1, 4);
    ASSERT_EQ(*e, 2);
    ASSERT_EQ(x.size(), 100);
    x.pop_front();
    iterator b2 = begin(x);
    ASSERT_EQ(*b2, 2);
    ASSERT_EQ(*e, 2);
    ASSERT_EQ(x.size(), 99);
}

TYPED_TEST(DequeFixture, pop_back) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(99, 2);
    iterator b = begin(x);
    x.push_back(4);
    x.push_back(6);
    ASSERT_EQ(x.size(), 101);
    x.pop_back();
    iterator e1 = end(x);
    --e1;
    ASSERT_EQ(*e1, 4);
    ASSERT_EQ(*b, 2);
    ASSERT_EQ(x.size(), 100);
    x.pop_back();
    iterator e2 = end(x);
    --e2;
    ASSERT_EQ(*e2, 2);
    ASSERT_EQ(*b, 2);
    ASSERT_EQ(x.size(), 99);
}

TYPED_TEST(DequeFixture, insert) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(99, 2);
    iterator e1 = x.end();
    x.insert(e1, 6);
    ASSERT_EQ(*e1, 6);
    ASSERT_EQ(x.size(), 100);
    iterator e2 = x.end();
    --e2;
    x.insert(e2, 4);
    ASSERT_EQ(*e2, 4);
    ASSERT_EQ(x.size(), 101);
    ++e1;
    ASSERT_EQ(*e1, 6);
}

TYPED_TEST(DequeFixture, erase) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(99, 2);
    x.push_back(4);
    x.push_back(6);
    iterator e1 = x.end();
    --e1;
    --e1;
    x.erase(e1);
    iterator e2 = x.end();
    --e2;
    ASSERT_EQ(*e2, 6);
    ASSERT_EQ(x.size(), 100);
    x.erase(e2);
    EXPECT_TRUE(e2 == x.end());
    ASSERT_EQ(x.size(), 99);
    iterator e3 = x.end();
    --e3;
    ASSERT_EQ(*e3, 2);
}

TYPED_TEST(DequeFixture, push_back_empty) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(4);
    ASSERT_EQ(x.size(), 1);
    x.push_back(6);
    ASSERT_EQ(x.size(), 2);
    iterator e = x.end();
    --e;
    ASSERT_EQ(*e, 6);
    --e;
    ASSERT_EQ(*e, 4);
}

TYPED_TEST(DequeFixture, push_front_empty) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_front(4);
    ASSERT_EQ(x.size(), 1);
    x.push_front(6);
    ASSERT_EQ(x.size(), 2);
    iterator e = x.end();
    --e;
    ASSERT_EQ(*e, 4);
    --e;
    ASSERT_EQ(*e, 6);
}

TYPED_TEST(DequeFixture, constructors) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x1(5, 2);
    deque_type x2({2, 2, 2, 2, 2});
    ASSERT_EQ(x1, x2);
}

TYPED_TEST(DequeFixture, pop_zero_edge) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x1(1, 1);
    x1.pop_back();
    ASSERT_EQ(x1.size(), 0);
}

TYPED_TEST(DequeFixture, resize) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x1({1, 2, 3, 4, 5});
    x1.resize(3);
    deque_type x2({1, 2, 3});
    ASSERT_EQ(x1, x2);
    deque_type x3(70, 2);
    x3.resize(240);
    deque_type x4(70, 2);
    for(int i = 0; i < 170; i++) {
        x4.push_back(0);
    }
    ASSERT_EQ(x3, x4);
}

TYPED_TEST(DequeFixture, copy_equal) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x1({1, 2, 3, 4, 5});
    deque_type x2;
    x2 = x1;
    ASSERT_EQ(x1, x2);
}

TYPED_TEST(DequeFixture, indexing1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({5, 4, 3, 2, 1});
    ASSERT_EQ(x[2], 3);
}

TYPED_TEST(DequeFixture, indexing2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({5, 4, 3, 2, 1});
    x[2] = 999;
    ASSERT_EQ(x[2], 999);
}

TYPED_TEST(DequeFixture, at) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({5, 4, 3, 2, 1});
    ASSERT_EQ(x.at(2), 3);
}

TYPED_TEST(DequeFixture, at2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({5, 4, 3, 2, 1});
    x.at(2) = 999;
    ASSERT_EQ(x.at(2), 999);
}

TYPED_TEST(DequeFixture, back) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({5, 4, 3, 2, 1});
    ASSERT_EQ(x.back(), 1);
}

TYPED_TEST(DequeFixture, front) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({5, 4, 3, 2, 1});
    ASSERT_EQ(x.front(), 5);
}