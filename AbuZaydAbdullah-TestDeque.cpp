// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>      // deque
#include <stdexcept>  // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
  using deque_type = T;
  using allocator_type = typename deque_type::allocator_type;
  using iterator = typename deque_type::iterator;
  using const_iterator = typename deque_type::const_iterator;
};

using deque_types = Types<deque<int>, my_deque<int>, deque<int, allocator<int>>,
                          my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types, );
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
  using deque_type = typename TestFixture::deque_type;
  const deque_type x;
  ASSERT_TRUE(x.empty());
  EXPECT_TRUE(x.size() == 0u);
}

TYPED_TEST(DequeFixture, test2) {
  using deque_type = typename TestFixture::deque_type;
  using iterator = typename TestFixture::iterator;
  deque_type x;
  iterator b = begin(x);
  iterator e = end(x);
  EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test3) {
  using deque_type = typename TestFixture::deque_type;
  using const_iterator = typename TestFixture::const_iterator;
  const deque_type x;
  const_iterator b = begin(x);
  const_iterator e = end(x);
  EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test4) {
  using deque_type = typename TestFixture::deque_type;
  using iterator = typename TestFixture::iterator;
  deque_type x(10);
  iterator b = begin(x);
  iterator e = end(x);
  EXPECT_TRUE(b != e);
}

TYPED_TEST(DequeFixture, test5) {
  using deque_type = typename TestFixture::deque_type;
  using iterator = typename TestFixture::iterator;
  deque_type x(10, 5);
  iterator b = begin(x);
  iterator e = end(x);
  int counter = 0;
  while (b != e) {
    ++counter;
    ++b;
  }
  EXPECT_TRUE(counter == 10);
}

TYPED_TEST(DequeFixture, test6) {
  using deque_type = typename TestFixture::deque_type;
  using iterator = typename TestFixture::iterator;
  deque_type x(100, 5);
  iterator b = begin(x);
  iterator e = end(x);
  int counter = 0;
  while (b != e) {
    ++counter;
    ++b;
  }
  EXPECT_TRUE(counter == 100);
}

TYPED_TEST(DequeFixture, test7) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(344, 9932);
  EXPECT_TRUE(x.front() == 9932);
}

TYPED_TEST(DequeFixture, test8) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(11, false);
  EXPECT_TRUE(x.back() == false);
}

TYPED_TEST(DequeFixture, test9) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(41, true);
  try {
    EXPECT_TRUE(x.at(41) == true);
    EXPECT_TRUE(false);
  } catch (std::out_of_range const& err) {
    EXPECT_TRUE(true);
  }
}

TYPED_TEST(DequeFixture, test10) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(41, true);
  EXPECT_TRUE(x.at(40) == true);
}

TYPED_TEST(DequeFixture, test11) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(40, true);
  x.resize(4);
  EXPECT_TRUE(x.size() == 4);
}

TYPED_TEST(DequeFixture, test12) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(65, true);
  deque_type y(7, false);
  x.swap(y);
  EXPECT_TRUE(x.size() == 7);
  EXPECT_TRUE(y.size() == 65);
}

TYPED_TEST(DequeFixture, test13) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x({13, 1, 87, 104});
  EXPECT_TRUE(x[0] == 13);
  EXPECT_TRUE(x[2] == 87);
}

TYPED_TEST(DequeFixture, test14) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x({41, 8133, 79, 432});
  EXPECT_TRUE(x.back() == 432);
  x.pop_back();
  EXPECT_TRUE(x.back() == 79);
  x.pop_back();
  EXPECT_TRUE(x.back() == 8133);
  x.pop_back();
  EXPECT_TRUE(x.back() == 41);
}

TYPED_TEST(DequeFixture, test15) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x({41, 8133, 79, 432});
  EXPECT_TRUE(x.front() == 41);
  x.pop_front();
  EXPECT_TRUE(x.front() == 8133);
  x.pop_front();
  EXPECT_TRUE(x.front() == 79);
  x.pop_front();
  EXPECT_TRUE(x.front() == 432);
}

TYPED_TEST(DequeFixture, test16) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x;
  x.push_front(11);
  EXPECT_TRUE(x.front() == 11);
}

TYPED_TEST(DequeFixture, test17) {
  using deque_type = typename TestFixture::deque_type;
  using iterator = typename TestFixture::iterator;
  deque_type x(9);
  iterator b = begin(x);
  iterator e = end(x);
  b += 9;
  EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test18) {
  using deque_type = typename TestFixture::deque_type;
  using iterator = typename TestFixture::iterator;
  deque_type x(9);
  iterator b = begin(x);
  iterator e = end(x);
  e -= 9;
  EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test19) {
  using deque_type = typename TestFixture::deque_type;
  using iterator = typename TestFixture::iterator;
  deque_type x(6);
  iterator b = begin(x);
  iterator e = end(x);
  b += 3;
  e -= 3;
  EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test20) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x;
  x.push_back(100);
  EXPECT_TRUE(x[0] == 100);
}

TYPED_TEST(DequeFixture, test21) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(43, 1);
  x.push_back(87);
  EXPECT_TRUE(x[43] == 87);
}

TYPED_TEST(DequeFixture, test22) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(43, 1);
  x.push_front(87);
  EXPECT_TRUE(x[0] == 87);
}

TYPED_TEST(DequeFixture, test23) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(43, 1);
  x.clear();
  EXPECT_TRUE(x.size() == 0);
}

TYPED_TEST(DequeFixture, test24) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(43, 1);
  x.clear();
  x.push_back(11);
  EXPECT_TRUE(x.size() == 1);
}

TYPED_TEST(DequeFixture, test25) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x({1});
  x.erase(begin(x));
  EXPECT_TRUE(x.size() == 0);
}

TYPED_TEST(DequeFixture, test26) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x({1, 67});
  x.erase(begin(x));
  EXPECT_TRUE(x[0] == 67);
}

TYPED_TEST(DequeFixture, test27) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x({1, 67});
  x.erase(begin(x));
  EXPECT_TRUE(x.size() == 1);
}

TYPED_TEST(DequeFixture, test28) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x({1, 67});
  x.insert(begin(x), 100);
  EXPECT_TRUE(x.size() == 3);
}

TYPED_TEST(DequeFixture, test29) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x({1, 67});
  x.insert(begin(x), 100);
  EXPECT_TRUE(x[0] == 100);
}

TYPED_TEST(DequeFixture, test30) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x({1, 67});
  x.insert(end(x), 100);
  EXPECT_TRUE(x[2] == 100);
}

TYPED_TEST(DequeFixture, test31) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x;
  deque_type y(7, false);
  x.swap(y);
  EXPECT_TRUE(x.size() == 7);
  EXPECT_TRUE(y.size() == 0);
}

TYPED_TEST(DequeFixture, test32) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x;
  deque_type y(7, false);
  x = y;
  EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, test33) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(7, 4);
  deque_type y(7, 9);
  EXPECT_TRUE(x < y);
}

TYPED_TEST(DequeFixture, test34) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(8, 4);
  deque_type y(7, 9);
  EXPECT_TRUE(x < y);
}

TYPED_TEST(DequeFixture, test35) {
  using deque_type = typename TestFixture::deque_type;
  deque_type x(8, 4);
  deque_type y(x);
  EXPECT_TRUE(x == y);
}
